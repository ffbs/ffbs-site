# https://gluon.readthedocs.io/en/v2019.1.x/user/site.html#build-configuration
GLUON_WLAN_MESH=11s

# enable factory and sysupgrade-images for all devices:
# see: https://gluon.readthedocs.io/en/latest/user/site.html#user-site-build-configuration
GLUON_DEPRECATED=full

DEFAULT_GLUON_RELEASE := $(shell ${GLUON_SITEDIR}/getRelease.sh)-$(shell date '+%Y%m%d')-snapshot

# Allow overriding the release number from the command line
GLUON_RELEASE ?= $(DEFAULT_GLUON_RELEASE)

GLUON_PRIORITY ?= 0

GLUON_REGION ?= eu
GLUON_LANGS ?= en de

GLUON_COLLECT_KERNEL_DEBUG=1
