features({
	'web-advanced',
	'web-osm',
	'config-mode-geo-location-osm',
	'web-cellular',
})

packages({
	'gluon-mesh-batman-adv-15',
	'gluon-autoupdater',
	'gluon-authorized-keys',
	'gluon-config-mode-autoupdater',
	'gluon-config-mode-core',
	'gluon-config-mode-geo-location',
	'gluon-config-mode-hostname',
	'gluon-config-mode-mesh-vpn',
	'gluon-config-mode-contact-info',
	'gluon-web-admin',
	'gluon-web-autoupdater',
	'gluon-web-wifi-config',
	'gluon-web-logging',
	'gluon-radvd',
	'gluon-setup-mode',
	'gluon-status-page',
	'gluon-web-private-wifi',
	'iwinfo',
	'gluon-web-network',
	'ffbs-mesh-vpn-parker',
	'ffbs-wireguard-respondd',
	'-gluon-ebtables-limit-arp',
	'gluon-ffbsnext-debugrsyslog',
	'gluon-ffbsnext-info-pull',
	'gluon-status-page-mesh-batman-adv',
	'respondd-module-airtime',
	'ffbs-debugbathosts',
	'ffbs-collect-debug-info',
	'ffac-weeklyreboot',
})

-- support acpi shutdown for openwrt as VM
if target('x86-generic') or target('x86-geode') or target('x86-64') then
	featrues({
		'kod-button-hotplug',
	})
end
